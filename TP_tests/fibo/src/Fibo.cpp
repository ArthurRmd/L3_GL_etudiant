#include "Fibo.hpp"
#include <assert.h>     /* assert */
#include "string"

int fibo(int n, int f0, int f1) {

    //assert( n < 0  );
    //assert( f0 > f1  );

    if ( n < 0  ) {
        throw std::string("erreur < 0");
    }

    if ( n >= 50  ) {
        throw std::string("erreur < 0");
    }
    return n<=0 ? f0 : fibo(n-1, f1, f1+f0);
}

