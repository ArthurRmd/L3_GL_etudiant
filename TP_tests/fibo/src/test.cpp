//
// Created by arthur on 27/04/2020.
//

#include <CppUTest/CommandLineTestRunner.h>
#include "Fibo.hpp"

TEST_GROUP(Fibo) {
};

TEST(Fibo, first_test) {


    CHECK_EQUAL(fibo(1), 1);
    CHECK_EQUAL(fibo(2), 1);
    CHECK_EQUAL(fibo(3), 2);
    CHECK_EQUAL(fibo(4), 3);
    CHECK_EQUAL(fibo(5), 5);

}

TEST(Fibo, Second_test) {

    CHECK_THROWS( std::string, fibo(55))
    CHECK_THROWS( std::string, fibo(-1))


}