"use strict";

function fiboNaive(n) {

    return (n < 2) ? n : fiboNaive(n - 1) + fiboNaive(n - 2)

}

function fiboIterative(n) {

    if (n < 2) return n

    let f0 = 0;
    let f1 = 1;

    for (let i = 1; i < n; i++)
        [f0 , f1] = [ f1, f0+f1]

    return f1;

}

